using Flux, DiffEqFlux, OrdinaryDiffEq
using Dates: today
using BSON: @save, @load
using StatsBase: shuffle!, mean

# robot = "pendulum.jl"
robot = "iwp.jl"


include("neural_ode.jl")
include(robot)
include("data_generation.jl")


## HYPER PARAMETERS
widths = [4 64 64 1]
dt = 1/20f0
n_samples = 401
n_iter = 30
epochs = 1
batch_size = 48
activation_fn = elu
output_activation = x -> *(typeof(x)(0.25), x) 
## END HYPER PARAMETERS


## CONSTANTS
tfinal = last(range(0, length=n_samples, step=dt)) |> Float32
plot_freq = isinteractive() ? 4 : NaN
tspan = (0f0, tfinal)


## DEFINING NETWORK
layer_type, net_type = FastDense, FastChain
layers = Tuple([
    layer_type(widths[i-1], widths[i], i==length(widths) ? output_activation : activation_fn,
            initW=Flux.glorot_uniform, initb=Flux.glorot_uniform) for i in 2:length(widths)
])
net = net_type(layers...)
x0 = 3f0*(rand(Float32, widths[1]) .- 0.5f0)
p = initial_params(net)
prob = ODEProblem(dynamics_tracker, x0, tspan, p)
# prob = ODEProblem(dynamics, x0, tspan, p)


## CREATE DIRECTORY FOR SAVING
wdir = "/home/wankun/Research/sp20/ml_based_esc/Julia"
savedir = wdir * "/data"
TODAY = today() # "2020-02-20"
lastID = findlast( x->occursin("$(TODAY)",x), readdir(savedir) )
if isnothing(lastID)
    nextID = 1
else
    nextID = 1 + parse(Int, readdir(savedir)[lastID][end-1:end])
end
savedir = savedir * "/$(TODAY)-$(string(nextID, pad=2))"
run(`mkdir -p $(savedir)`)


## WRITE TRAINING RECIPE
begpat = ["SYSTEM PARAMETERS", "HYPER PARAMETERS"]
endpat = ["END SYSTEM PARAMETERS", "END HYPER PARAMETERS"]
filpat = [robot, "batch_training.jl"]
run(`rm -f $(savedir)/recipe.txt`)
for i = 1:2
    run(
        pipeline(
            `awk '/## '$(begpat[i])'/,/## '$(endpat[i])'/' $(wdir)/$(filpat)`, 
            stdout="$(savedir)/recipe.txt", append=true
        )
    )
end


## HELPER FUNCTION FOR PRINTING
print_format = "Training:  %4d/%-4d  |  x0 = (" * join(["%7.4f, " for _=1:widths[1]])[1:end-2] * ")  |  loss = %8.4f  |  time = %3.2f\n"
@eval showprogress(args...) = @sprintf($print_format, args...)


## TRAINING
outfilename = "$(savedir)/output.txt"
io = open(outfilename, "a") 
training_data = Tuple{Array{Float32}, Array{Float32}}[]
last_iter = 1
try
    for i = (1:n_iter)
        last_iter = i
        @info("DAgger iteration #$(i). Generating labels....")
        write(io, "DAgger iteration #$(i)\n")
        dagger_x0 = 2f0*(rand(Float32, widths[1]) .- 0.5f0)
        if rand() < 0.1
            if robot == "cartpole.jl"
                dagger_x0[3] = pif0 - 0.5f0*rand(Float32)
            else    
                dagger_x0[1] = pif0 - (rand(Float32) - 0.5f0)
            end
        end
        starting_points, starting_points_full = dagger_generate_x0(dynamics, p, dagger_x0, 7)
        [push!(training_data, (x, [0f0])) for x in starting_points.u]
        push!(training_data, (dagger_x0, [0f0]))
        shuffle!(training_data)

        counter   = 1
        cumloss   = 0.0
        bestloss  = Inf
        worstloss = -Inf
        for data in Iterators.partition(training_data, 4)
            x0s = getindex.(data,1)
            compute_time = @elapsed begin
                prediction, ctrl, loss = train_many!(p, x0s, homoclinic_loss, epochs, batch_size, verbose=!true);
            end
            current_loss = mean(loss)
            cumloss += current_loss
            if minimum(loss) < bestloss
                bestloss = minimum(loss)
            end
            if maximum(loss) > worstloss
                worstloss = maximum(loss)
            end
            for i = 1:length(x0s)
                progress = showprogress(
                    counter, ceil(length(training_data)/4), x0s[i][:,1]..., loss[i], compute_time
                )
                write(io, progress)
                print(progress)
            end
            if counter % plot_freq == 0
                create_plots(dt, prediction, p, ctrl, nothing, nothing, starting_points_full, starting_points.t)
            end
            counter += 1
        end
        @save "$(savedir)/$(string(i, pad=3)).bson" p
        summary = @sprintf(
            "Iteration #%d summary: mean loss = %9.4f, best loss = %9.4f, worst loss = %9.4f\n",
            i, cumloss/counter, bestloss, worstloss
        )
        println(summary); write(io, summary);
        print("\n")
        write(io, "\n")
    end
    
catch e
    if isa(e, InterruptException)
        @info("Training interrupted. Saving data...")
    else
        rethrow(e)
    end
finally
    @save "$(savedir)/$(string(last_iter, pad=3)).bson" p
    close(io)
    @save "$(savedir)/training_data.bson" training_data
end
