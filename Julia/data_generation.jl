using StatsBase: sample


function dagger_generate_x0(dynamics::Function, p::Vector{Float32}, x0::Vector{Float32}, num_traj)
    tspan = (0f0, Float32(tfinal))
    t = range(0f0, tfinal, length=301)
    prob = ODEProblem(dynamics, x0, tspan, p)
    sol = OrdinaryDiffEq.solve(prob, Tsit5())
    saveid = map(x -> abs(x[1]) <= 0.8*pi, sol(t).u)
    if !all(saveid)
        saveid = [1, 2]
    end
    tsave = t[ saveid ]     # Discard points with norm > 1.1*pi
    if length(tsave) <= num_traj
        return sol(tsave), sol
    else
        return sol(sample(tsave, num_traj, replace=false)), sol
    end
end


function dagger_collect_into!(D::Array{Tuple{Array{T}, Array{T}}}, 
                                starting_points::Array{Array{T,1}},
                                dt, n_samples) where {T<:AbstractFloat}
    for x0 in starting_points
        try
            xhat, uhat = generate_trajectory(x0, dt, n_samples, use_warm_start=true, verbose=false)
            push!(D, (xhat, uhat))
        catch MethodError
            @warn("Direct collocation did not converge for x0=$(x0). Discarding data point.")
        end
    end
end
