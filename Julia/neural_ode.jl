using Flux, DiffEqFlux, OrdinaryDiffEq, DiffEqSensitivity
using Optim
using Zygote: @nograd
using Random: randperm
using LinearAlgebra
using Printf
@nograd randperm
@nograd append!
LinearAlgebra.BLAS.set_num_threads(4)


## Helper functions and types
dtanh(x) = 1.0f0 - tanh(x)*tanh(x)
delu(x, α=one(x)) = ifelse(x > 0, one(x), elu(x, α) + α)
saturate(input, limit) = min(limit, max(-limit, input))
ParamVec = Union{DiffEqFlux.Tracker.TrackedArray, Vector{Float32}}
pif0 = 1f0*pi


function wrap2pi(x::Union{Real, DiffEqFlux.Tracker.TrackedReal})
    wrapMax(y, max) = mod(max + mod(y, max), max)
    wrapMinMax(y, min, max) = min + wrapMax(y - min, max - min)
    return wrapMinMax(x, -pi, pi)
    # atan(sin(x), cos(x))
end


function param2Wb(f::DiffEqFlux.FastDense, p::ParamVec)
    """
    Convert ParamVec `for a single dense layer` to weight and biases
    """
    W, b = p[reshape(1:(f.out*f.in),f.out,f.in)], p[(f.out*f.in+1):end]
    return W, b
end


function Wb(j::Integer, p::ParamVec)
    """
    Convert ParamVec `for full neural network at layer j` to weight and biases
    """
    if isa(net.layers[j], DiffEqFlux.FastDense)
        p_len(i::Integer) = net.layers[i].in * net.layers[i].out + net.layers[i].out
        if j == 1
            ids = 1
        else 
            ids = sum(p_len(i) for i = j-1:-1:1) + 1
        end
        W, b = param2Wb( net.layers[j], p[ ids:(ids+p_len(j)-1) ] )
    elseif isa(net.layers[j], Flux.Dense)
        W, b = net.layers[j].W, net.layers[j].b
    end
    W, b
end


function net_gradient(p::ParamVec, input)
    """
    Computes the gradient of a NN w.r.t the input
    """
    net.layers[1].σ == elu ? dσ = delu : nothing
    net.layers[1].σ == tanh ? dσ = dtanh : nothing
    net.layers[1].σ == sin ? dσ = cos : nothing
    net.layers[1].σ == cos ? dσ = x->-sin(x) : nothing
    N = length(net.layers)
    output_activation.( Wb(N, p)[1] * prod( dσ.(layer_output(p, i, input)) .* Wb(i, p)[1] for i = N-1:-1:1 ) )
end


function layer_output(p::ParamVec, layer::Integer, input)
    """
    Output of a NN before going through activation function, i.e. (Wx + b)
    """
    activation = input
    z = input
    for j = 1:layer
        W, b = Wb(j, p)
        z = W * activation + b
        activation = net.layers[j].σ.(z)
    end
    z
end


function predict(p::ParamVec, x0::Vector{<:AbstractFloat})
    """
    Integrate the system forward with sensitivity analysis for updating weights
    """
    if prob.f.f == dynamics
        sensealg = DiffEqSensitivity.InterpolatingAdjoint()
    elseif prob.f.f == dynamics_tracker
        sensealg = DiffEqSensitivity.TrackerAdjoint()
    end
    Array( 
        concrete_solve( prob, Tsit5(), x0, p, saveat=dt, sensealg=sensealg ) 
    )
end


function forward_pass(p::ParamVec, x0::Vector{<:AbstractFloat})
    """
    Integrate the system forward and plot. Use for evaluation of current NN params.
    """
    _prob = remake(prob, u0=x0, p=p)
    sol = OrdinaryDiffEq.solve(_prob, Tsit5(), saveat=0.1)
    u = [compute_control(p, x) for x in sol.u]
    create_plots(dt, Array(sol), p, u)
end


function train_once!(p::Vector{Float32}, x0::Vector{Float32}, raw_loss::Function, N_iter::Integer=8, batch_size=64; verbose=false)
    current_loss = 0f0
    current_prediction = Array{Float32, 2}
    current_ctrl = Vector{Float32}
    samples = randperm(length(0f0:dt:tfinal))[1:batch_size]
    function loss(p)
        x = predict(p, x0)        
        u = [compute_control(p, x[:,i]) for i in 1:length(x[1,:])]
        myloss = raw_loss(x, u, samples)
        current_loss = myloss
        current_prediction = x
        current_ctrl = u
        regularization = sqrt(sum(abs2, p))
        sum(myloss) + 0.5f0*regularization
    end
    function cb(ϕ, loss)
        ret = false
        if verbose
            header = @sprintf("")
            msg = @sprintf("")
            for i = 1:length(current_loss)
                header = string(header, @sprintf("|      loss[%d]  ", i))
                msg = string(msg, @sprintf("|  %11.4f  ", current_loss[i]))
            end
            header = string(header, "|\n")
            msg = string(msg, "|\n")
            print(header); print(msg)
        end
        if any(isnan.(ϕ))
            @warn("params are NaN, skipping training")
            ret = true
        end
        return ret
    end

    res = DiffEqFlux.sciml_train(loss, p, ADAM(), cb=cb, maxiters=N_iter)
    if !any(isnan.(res.minimizer))
        p[:] = res.minimizer;
    end

    return current_prediction, current_ctrl, sum(current_loss)
end


function train_many!(p::Vector{Float32}, x0s::Vector{Vector{Float32}}, raw_loss::Function, N_iter::Integer=8, batch_size=64; verbose=false)
    current_loss = Vector{Float32}()
    current_prediction = Array{Float32, 2}
    current_ctrl = Vector{Float32}
    samples = randperm(length(0f0:dt:tfinal))[1:batch_size]
    function loss(p)
        total = 0f0
        for x0 in x0s
            x = predict(p, x0)        
            u = [compute_control(p, x[:,i]) for i in 1:length(x[1,:])]
            myloss = raw_loss(x, u, samples)
            append!(current_loss, sum(myloss))
            current_prediction = x
            current_ctrl = u
            regularization = sqrt(sum(abs2, p))
            total += sum(myloss) + 0.5f0*regularization
        end
        total / length(x0s)
    end
    function cb(ϕ, loss)
        ret = false
        if verbose
            header = @sprintf("")
            msg = @sprintf("")
            for i = 1:length(current_loss)
                header = string(header, @sprintf("|      loss[%d]  ", i))
                msg = string(msg, @sprintf("|  %11.4f  ", current_loss[i]))
            end
            header = string(header, "|\n")
            msg = string(msg, "|\n")
            print(header); print(msg)
        end
        if any(isnan.(ϕ))
            @warn("params are NaN, skipping training")
            ret = true
        end
        return ret
    end

    res = DiffEqFlux.sciml_train(loss, p, ADAM(), cb=cb, maxiters=N_iter)
    if !any(isnan.(res.minimizer))
        p[:] = res.minimizer;
    end

    return current_prediction, current_ctrl, current_loss
end
