using OrdinaryDiffEq
using Plots; pyplot()
using LaTeXStrings


## SYSTEM PARAMETERS
MAX_TORQUE = 2.5*52.5*1e-3 |> Float32
MASS = 0.377f0      # weight including pivot rod [kg]
LEN_CM = 0.232f0    # length from pivot to center of mass [m]
LEN_TOT = 0.42f0    # length from pivot to point mass [m]
INERT_CM = 1/12f0*MASS*LEN_TOT^2  
INERT = (INERT_CM + MASS*LEN_CM^2) |> Float32
GRAVITY = 9.81f0
FRICTION_COEFF = 0.005f0
GEAR_RATIO = 13f0/3f0
mgl = MASS*GRAVITY*LEN_CM
# Kp = 1.15f0
# Kd = Kp / sqrt(GRAVITY/LEN_CM)
# Kp, Kd = 0.596803383626355f0, 31.448688716242852f0 # LQR gains with diag(Q) = [4, 100] and R = 0.1
# Kp, Kd = 1.747357389299812f0, 31.485029185644784f0
Kp, Kd = 0.462809488319079f0,  3.117507333157784f0
## END SYSTEM PARAMETERS


function compute_control(p::ParamVec, x)
    u = saturate(net_gradient(p, x) |> sum, MAX_TORQUE)
end


function dynamics(du, u, p, t)
    ctrl  = GEAR_RATIO*compute_control(p, u)
    du[1] = u[2]
    du[2] = (-mgl*sin(u[1]) - FRICTION_COEFF*u[2] + ctrl) / INERT
end


function dynamics_tracker(u, p, t)
    ctrl = GEAR_RATIO*compute_control(p, u)
    [
        u[2],
        (-mgl*sin(u[1]) - FRICTION_COEFF*u[2] + ctrl) / INERT
    ]
end


function forward_pass_switching(p::ParamVec, x0::Vector{<:AbstractFloat}, controller::Function; do_plot=false)
    function _ctrl(x::Vector{<:AbstractFloat})
        """
        This compute_control switches to the linear controller when pendulum is close to upright.
        For use during forward pass only. Not compatible with training.
        """
        θ, θdot = x
        pos_error = rem2pi(θ-pi, RoundNearest)
        vel_error = θdot
        if sqrt(pos_error^2 + vel_error^2) <= 0.5f0
            u = saturate(-Kp*pos_error - Kd*vel_error, MAX_TORQUE)
        else
            u = saturate(controller(x), MAX_TORQUE)
        end
        u
    end
    function _f(du, u, p, t)
        ctrl = GEAR_RATIO*_ctrl(u)
        du[1] = u[2]
        du[2] = (-mgl*sin(u[1]) - FRICTION_COEFF*u[2] + ctrl) / INERT
    end
    _prob = ODEProblem(_f, x0, tspan, p)
    sol = OrdinaryDiffEq.solve(_prob, Tsit5(), saveat=0.01)
    if any( x->isapprox([abs(rem2pi(x[1], RoundNearest)), x[2]], [pif0, 0f0], rtol=0.25), sol.u )
        success = true
    else
        success = false
    end
    u = [_ctrl(x) for x in sol.u]
    # loss = sum( norm.([x .- vcat(pif0, 0f0) for x in sol.u]) ) + norm(u)
    loss = norm(u)
    # loss = homoclinic_loss(Array(sol), u)
    if do_plot
        create_plots(0.01, Array(sol), p, u)
    end
    return sum(loss), success
end


function homoclinic_loss(x, u, xhat=nothing, uhat=nothing, R=0.05f0)
    θdot_star(θ) = begin
        E0 = -mgl*cos(pif0)
        sqrt( 2/INERT*(E0 + mgl*cos(θ)) )
    end
    x_perp(θ, θdot) = begin
        min(abs2(θdot - θdot_star(θ)), abs2(θdot + θdot_star(θ)))
    end
    N = length(x[1,:])
    samples = randperm(N)#[1:64]
    
    homoclinic_loss = 1f0 * sum( x_perp.(x[1,samples], x[2,samples]) ) / length(samples)
    ctrl_effort_loss = R*sum(abs2,u)

    h(x, r=0.1f0) = begin
        pos_error = rem2pi.(x[1,:] .- pif0, RoundNearest)
        delta = sqrt.(pos_error.^2 .+ x[2,:].^2)
        ret = minimum(delta)
        ifelse( ret <= r, 0f0, ret-r)
    end
    function find_sign_changes(x)
        findall( x->x>=2, abs.(diff(sign.(x))) )
    end
    x_crossings = x[ 2, find_sign_changes(x[1,:]) ]
    num_crossings = min(5, length(x_crossings))
    dist_to_ball_penalty = num_crossings*h(x)^2

    hinge_θ(x) = 0.05f0*max(-x-pif0, x-pif0, zero(x))
    hinge_penalty = dist_to_ball_penalty*sum(hinge_θ.(x[1,:]))

    [homoclinic_loss, dist_to_ball_penalty, hinge_penalty, ctrl_effort_loss]
end


linestyles = [(2,:black), (2,:darkorange2), (1, :black, :dash)]
figsize = (800, 480)
upscale = 2.75
# fntstyle = "Latin Modern Math"
fntstyle = "sans-serif"
# fntstyle = "serif"
PyPlot.rc("text", usetex = "true")
PyPlot.rc("font", family = "Latin Modern Math")
# PyPlot.rc("axes", labelweight = "bold") 
fntsm = Plots.font(fntstyle, pointsize=round(9.0*upscale))
fntlg = Plots.font(fntstyle, pointsize=round(13.0*upscale))
default(titlefont=fntlg, guidefont=fntsm, tickfont=fntsm, legendfont=fntsm, tickfontfamily=fntsm, xtickfontfamily=fntsm)
default(framestyle=:box)
# default(titlefont=fntlg, guidefont=fntlg, tickfont=fntsm, legendfont=fntsm)
default(size=(1200,600)) #Plot canvas size
# default(dpi=round(72)) #Only for PyPlot - presently broken
# default(dpi=100) 


function create_plots(dt::Real, x::Array{T,2}, 
                        par::Union{ParamVec,Nothing}=nothing,
                        u::Union{Array{T,1},Nothing}=nothing, 
                        xhat::Union{Array{T,2},Nothing}=nothing, 
                        uhat::Union{Array{T,1},Nothing}=nothing, 
                        x0_dagger::Union{ODESolution,Nothing}=nothing, 
                        tsave::Union{Array{T,1},Nothing}=nothing) where {T<:AbstractFloat}
    
    myrange = 1:1000
    θ = x[1,myrange]
    θdot = x[2,myrange]
    # t = range(0, stop=dt*(length(θ)-1), step=dt)
    t = range(0, stop=15, length=length(θ))
    u = [compute_control(p, [x[1,i], x[2,i]]) for i = 1:length(x[1,myrange])]
    N = 1; Npi = N*pi
    pi_ticks = -Npi:pi:Npi
    # pi_ticklabels = ["$(i)" * L"\pi" for i = -N:N]
    pi_ticklabels = [L"-\pi", L"0", L"\pi"]
    pi_axlim = [-Npi, Npi]

    p1 = plot(
        θ, θdot, line=linestyles[1], label="", 
        xlim=pi_axlim, xticks=(pi_ticks, pi_ticklabels),
        ylabel=L"Angular velocity $\dot{\theta}$",
        xlabel=L"Angle $\theta$", 
        title="State evolution",
        framestyle=:box, grid=:none
    );
    p2 = plot(
        t, u, line=linestyles[1], label="", 
        # xlim=pi_axlim, xticks=(pi_ticks, pi_ticklabels),
        title=L"Control input $u(t)$", 
        xlabel="Time (s)",
        # title="State evolution",
        framestyle=:box, grid=:none
    );

    θ_grid = range(-1π; stop=1π, length=20)
    θdot_grid = range(-11.0; stop=11.0, length=20)
    X = repeat(reshape(θ_grid, 1, :), length(θdot_grid), 1)
    Y = repeat(θdot_grid, 1, length(θ_grid))
    V = map((x,y)->net(vcat(x,y), par)[1], X, Y)
    U = map((x,y)->compute_control(par, vcat(x,y)), X, Y)

    # p6 = heatmap(
    #     θ_grid, θdot_grid, U,
    #     # levels=8,
    #     clims=(-0.025, 0.025),
    #     framestyle=:box,
    #     xlim=pi_axlim, xticks=(pi_ticks, pi_ticklabels),c=:coolwarm, grid=:none,
    #     # grid=(:dot,5,1.0), c=:coolwarm, fill=true, label=L"u(x)",
    #     # colorbar=:bottom,
    #     title=L"Learned policy $u(x)$", 
    #     # xlabel=L"Angle $\theta$", ylabel=L"Angular velocity $\dot{\theta}$",#legend=:none,
    #     # titlefontsize=fontsize_L, guidefontsize=fontsize_M, tickfontsize=fontsize_S
    # )


    display(
        plot(
            p1,p2,
            # xlabel=L"Angle $\theta$",
            layout=(1,2)
        )
    )   # slow (1.05 s)

end


function create_plots_bak(dt::Real, x::Array{T,2}, 
                        par::Union{ParamVec,Nothing}=nothing,
                        u::Union{Array{T,1},Nothing}=nothing, 
                        xhat::Union{Array{T,2},Nothing}=nothing, 
                        uhat::Union{Array{T,1},Nothing}=nothing, 
                        x0_dagger::Union{ODESolution,Nothing}=nothing, 
                        tsave::Union{Array{T,1},Nothing}=nothing) where {T<:AbstractFloat}
    θ = x[1,:]
    θdot = x[2,:]
    t = range(0, stop=dt*(length(θ)-1), step=dt)

    fontsize_M = 14
    fontsize_S = 10

    N = 5; Npi = N*pi
    pi_ticks = -Npi:pi:Npi
    pi_ticklabels = ["$(i)" * L"\pi" for i = -N:N]
    pi_axlim = [-Npi, Npi]

    p1 = plot(
        θ, θdot, line=linestyles[1], label="", 
        xlim=pi_axlim, xticks=(pi_ticks, pi_ticklabels),
        xlabel=L"\theta", ylabel=L"\dot{\theta}",
        guidefontsize=fontsize_M, tickfontsize=fontsize_S
    );
    p2 = plot(
        t, θ, label="q", line=linestyles[1],
        yticks=(pi_ticks, pi_ticklabels), ylim=pi_axlim,
        xlabel=L"t (s)", ylabel=L"\theta",
        guidefontsize=fontsize_M, tickfontsize=fontsize_S
    );
    p3 = plot(
        t, θdot, line=linestyles[1], label="",
        xlabel=L"t (s)", ylabel=L"\dot{\theta}",
        guidefontsize=fontsize_M, tickfontsize=fontsize_S);
        p4 = plot(t, u, line=linestyles[1], label="",
        xlabel=L"t (s)", ylabel=L"u",
        guidefontsize=fontsize_M, tickfontsize=fontsize_S
    );
    if !any( (isnothing(xhat), isnothing(uhat)) )
        plot!(p1, xhat[1,:], xhat[2,:], line=linestyles[2], label="desired_states");
        plot!(p2, t, xhat[1,:], line=linestyles[2], label="desired_q");
        plot!(p3, t, xhat[2,:], line=linestyles[2], label="desired_qdot");
        plot!(p4, t, uhat, line=linestyles[2], label="u_desired");
    end
    if !any( (isnothing(x0_dagger), isnothing(tsave)) )
        θ0(t) = x0_dagger(t)[1]
        θdot0(t) = x0_dagger(t)[2]
        plot!(p1, θ0.(t), θdot0.(t), line=linestyles[3], alpha=0.4, label="starting_points");
        scatter!(p1, θ0.(tsave), θdot0.(tsave), marker=(:o,4), alpha=0.4, label="");
    end
    draw_circle!(p1)

    if true
        display(plot(p1,p2,p3,p4,layout=(4,1),size=figsize))
    else
        # These 5 lines below are slow (~0.6 s to execute)
        θ_grid = range(-6π*1.1; stop=6π*1.1, length=100)
        θdot_grid = range(-6.0; stop=6.0, length=100)
        X = repeat(reshape(θ_grid, 1, :), length(θdot_grid), 1)
        Y = repeat(θdot_grid, 1, length(θ_grid))
        V = map((x,y)->net(vcat(x,y), par)[1], X, Y)
        U = map((x,y)->compute_control(par, vcat(x,y)), X, Y)

        # p5 = surface(
        #     X, Y, V,
        #     # xlabel="q", ylabel="qdot", zlabel="V(x)",
        #     grid=(:dot,5,1.0), c=:coolwarm, cbar=:none,
        #     xlabel=L"\theta", ylabel=L"\dot{\theta}", zlabel=L"V(x)",
        #     guidefontsize=fontsize_M, tickfontsize=fontsize_S
        # )
        # p6 = surface(
        #     X, Y, U,
        #     # xlabel="q", ylabel="qdot", zlabel="u(x)",
        #     grid=(:dot,5,1.0), c=:coolwarm, cbar=:none,
        #     xlabel=L"\theta", ylabel=L"\dot{\theta}", zlabel=L"V(x)",
        #     guidefontsize=fontsize_M, tickfontsize=fontsize_S
        # )
        p5 = contour(
        X, Y, V,
            # xlabel="q", ylabel="qdot", zlabel="V(x)",
            grid=(:dot,5,1.0), c=:coolwarm, cbar=:none, fill=true, label=L"V(x)",
            xlabel=L"\theta", ylabel=L"\dot{\theta}", zlabel=L"V(x)",
            guidefontsize=fontsize_M, tickfontsize=fontsize_S
        )
        p6 = contour(
        X, Y, U,
            # xlabel="q", ylabel="qdot", zlabel="u(x)",
            grid=(:dot,5,1.0), c=:coolwarm, cbar=:none, fill=true, label=L"u(x)",
            xlabel=L"\theta", ylabel=L"\dot{\theta}", zlabel=L"V(x)",
            guidefontsize=fontsize_M, tickfontsize=fontsize_S
        )


        mylayout = @layout [ grid(4,1) [ a{0.5h}
                            b{0.5h}] ] 
        display(plot(p1,p2,p3,p4,p5,p6,layout=mylayout,size=figsize))   # slow (1.05 s)
    end

end


function draw_circle!(p, r=0.25)
    x = π-r:0.01:π+r
    y = -r:0.01:r
    f(x, y) = (x-π)^2 + y^2 - r^2
    X = repeat(reshape(x, 1, :), length(y), 1)
    Y = repeat(y, 1, length(x))
    Z = map(f, X, Y)
    contour!(p, x, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+2*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+2*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+4*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+4*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+6*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+6*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
end 
