using OrdinaryDiffEq
using Plots; pyplot()
using LaTeXStrings


## SYSTEM PARAMETERS
INERT_PEND = 0.1f0    
INERT_DISK = 0.2f0           
mgl = 10f0
MAX_TORQUE = 1.5f0
## END SYSTEM PARAMETERS


function compute_control(p::ParamVec, x)
    q1, q1dot, q2, q2dot = x
    grad = net_gradient(p, x)
    u = -(grad[1] - grad[3])*1f0
    saturate(u, MAX_TORQUE)
end


function dynamics(du, u, p, t)
    q1, q1dot, q2, q2dot = u
    ctrl = compute_control(p, u)
    du[1] = q1dot
    du[2] = 1/INERT_PEND*(-mgl*sin(q1) - ctrl)
    du[3] = q2dot
    du[4] = 1/INERT_DISK*ctrl
end


function dynamics_tracker(u, p, t)
    q1, q1dot, q2, q2dot = u
    ctrl = compute_control(p, u)
    [
        q1dot,
        1/INERT_PEND*(-mgl*sin(q1) - ctrl),
        q2dot,
        1/INERT_DISK*ctrl
    ]
end


function forward_pass_switching(p::ParamVec, x0::Vector{<:AbstractFloat}; do_plot=false)
    function _ctrl(p, u)
        q1, q1dot, q2, q2dot = u
        q1_error = rem2pi(q1-pif0, RoundNearest)
        if abs(q1_error) >= 0.5 #sqrt(q1_error^2 + q1dot^2 + q2dot^2) > f0
        # if sqrt(q1_error^2 + q1dot^2) > 1f0
            ctrl = compute_control(p, u)
        else
            kpp, kdp, _, kdr = -377.23, -47.064, -1, -12.543
            ctrl = -kpp*q1_error - kdp*q1dot - kdr*q2dot
        end
        # ctrl = compute_control(p, u)
        ctrl = saturate(ctrl, MAX_TORQUE) |> Float32
    end
    function _f(du, u, p, t)
        q1, q1dot, q2, q2dot = u
        ctrl = _ctrl(p, u)
        du[1] = u[2]
        du[2] = 1/INERT_PEND*(-mgl*sin(u[1]) - ctrl)
        du[3] = u[4]
        du[4] = 1/INERT_DISK*ctrl
    end
    _prob = ODEProblem(_f, x0, tspan, p)
    _dt = 0.01
    sol = OrdinaryDiffEq.solve(_prob, Tsit5(), saveat=_dt)
    if any( x->isapprox([abs(rem2pi(x[1], RoundNearest)), x[2], x[4]], [pif0, 0f0, 0f0], atol=0.1), sol.u )
        success = true
    else
        success = false
    end
    u = [_ctrl(p, x) for x in sol.u]
    if do_plot
        create_plots(_dt, Array(sol), p, u)
    end
    return norm(u), success
end


function homoclinic_loss(x, u, samples, R=0.1f0)
    
    θdot_star(θ) = begin
        E0 = 2f0*mgl
        sqrt( 2/INERT_PEND*(E0 - mgl*(1-cos(θ))) )
    end
    x_perp(θ, θdot) = begin
        min(abs2(θdot - θdot_star(θ)), abs2(θdot + θdot_star(θ)))
    end
    h(x, r=0.05f0) = begin
        pos_error = rem2pi.(x[1,:] .- pif0, RoundNearest)
        delta = sqrt.(pos_error.^2 .+ x[2,:].^2 + x[4,:].^2)
        ret = minimum(delta)
        ifelse( ret <= r, 0f0, ret-r)
    end

    homoclinic_loss = 1f0 * sum( x_perp(x[1,i], x[2,i]) for i = samples ) / length(samples)
    set_distance_loss = 5f0*h(x)^2
    theta2_loss = 0.5f0*(sum(abs2, x[3,:]) + sum(abs2, x[4,:])) / length(x[3,:])
    ctrl_effort_loss = R*sum(abs2,u)
    hinge_θ(x) = 0.1f0*max(-x-pif0, x-pif0, zero(x))
    hinge_penalty = sum(hinge_θ.(x[1,:])) / length(x[1,:])
    
    [homoclinic_loss, set_distance_loss, theta2_loss, ctrl_effort_loss, hinge_penalty]
end


linestyles = [(2,:black), (2,:darkorange2), (1, :black, :dash)]
figsize = (1280, 1475)


function create_plots(dt::Real, x::Array{T,2}, 
                        par::Union{ParamVec,Nothing}=nothing,
                        u::Union{Array{T,1},Nothing}=nothing, 
                        xhat::Union{Array{T,2},Nothing}=nothing, 
                        uhat::Union{Array{T,1},Nothing}=nothing, 
                        x0_dagger::Union{ODESolution,Nothing}=nothing, 
                        tsave::Union{Array{T,1},Nothing}=nothing) where {T<:AbstractFloat}
    θ = x[1,:]
    θdot = x[2,:]
    t = range(0, stop=dt*(length(θ)-1), step=dt)

    N = 5; Npi = N*pi
    pi_ticks = -Npi:pi:Npi
    pi_ticklabels = ["$(i)" * L"\pi" for i = -N:N]
    pi_axlim = [-Npi, Npi]

    p1 = plot(θ, θdot, line=linestyles[1], label="states", xlim=pi_axlim, xticks=(pi_ticks, pi_ticklabels));
    _p2_1 = plot(t, θ, label="q1", line=linestyles[1],
                    yticks=(pi_ticks, pi_ticklabels), ylim=pi_axlim);
    _p2_2 = plot(t, x[2,:], line=linestyles[1], label="q1dot");
    p2 = plot(_p2_1, _p2_2, layout=(1,2))
    _p3_1 = plot(t, x[3,:], line=linestyles[1], label="q2");
    _p3_2 = plot(t, x[4,:], line=linestyles[1], label="q2dot");
    p3 = plot(_p3_1, _p3_2, layout=(1,2))
    p4 = plot(t, u, line=linestyles[1], xlabel="t (s)", label="u");

    if !any( (isnothing(xhat), isnothing(uhat)) )
        plot!(p1, xhat[1,:], xhat[2,:], line=linestyles[2], label="desired_states");
        plot!(p2, t, xhat[1,:], line=linestyles[2], label="desired_q");
        plot!(p3, t, xhat[2,:], line=linestyles[2], label="desired_qdot");
        plot!(p4, t, uhat, line=linestyles[2], label="u_desired");
    end
    if !any( (isnothing(x0_dagger), isnothing(tsave)) )
        θ0(t) = x0_dagger(t)[1]
        θdot0(t) = x0_dagger(t)[2]
        plot!(p1, θ0.(t), θdot0.(t), line=linestyles[3], alpha=0.4, label="starting_points");
        scatter!(p1, θ0.(tsave), θdot0.(tsave), marker=(:o,4), alpha=0.4, label="");
    end

    draw_circle!(p1)

    display(plot(p1,p2,p3,p4,layout=(4,1),size=figsize))

end




function create_plots_pub(dt::Real, x::Array{T,2}, 
                        par::Union{ParamVec,Nothing}=nothing,
                        u::Union{Array{T,1},Nothing}=nothing) where {T<:AbstractFloat}

    upscale = 2.5
    # fntstyle = "Latin Modern Math"
    fntstyle = "sans-serif"
    # fntstyle = "serif"
    PyPlot.rc("text", usetex = "true")
    PyPlot.rc("font", family = "Latin Modern Math")
    # PyPlot.rc("axes", labelweight = "bold") 
    fntsm = Plots.font(fntstyle, pointsize=round(10.0*upscale))
    fntlg = Plots.font(fntstyle, pointsize=round(14.0*upscale))
    default(titlefont=fntlg, guidefont=fntsm, tickfont=fntsm, legendfont=fntsm, tickfontfamily=fntsm, xtickfontfamily=fntsm)
    default(framestyle=:box)
    figsize = (1000, 1200)

    θ = x[1,:]
    θdot = x[2,:]
    t = range(0, stop=dt*(length(θ)-1), step=dt)

    N = 1; Npi = N*pi
    pi_ticks = -Npi:pi:Npi
    pi_ticklabels = ["$(i)" * L"\pi" for i = -N:N]
    pi_axlim = [-Npi, Npi]

    p1 = plot(
        θ, 
        θdot, 
        label="",
        line=linestyles[1], 
        xlim=pi_axlim, 
        xticks=(pi_ticks, pi_ticklabels),
        xlabel=L"\theta_1",
        ylabel=L"\dot{\theta}_1",
        title="State evolution",
        grid=:none
    );
    p3 = plot(
        t, 
        u,
        label="",
        line=linestyles[1], 
        xlabel="Time (s)",
        title="Control input",
        grid=:none
    );
    p2 = plot(
        t, 
        x[3,:], 
        label="",
        line=linestyles[1], 
        xlabel="Time (s)",
        title="Wheel position",
        grid=:none
    );
    p4 = plot(
        t, 
        x[4,:], 
        label="",
        line=linestyles[1], 
        xlabel="Time (s)",
        title="Wheel velocity",
        grid=:none
    );

    

    # draw_circle!(p1)

    display(plot(p1,p2,p3,p4,layout=(2,2),size=figsize))

end


function draw_circle!(p, r=0.1)
    x = π-r:0.01:π+r
    y = -r:0.01:r
    f(x, y) = (x-π)^2 + y^2 - r^2
    X = repeat(reshape(x, 1, :), length(y), 1)
    Y = repeat(y, 1, length(x))
    Z = map(f, X, Y)
    contour!(p, x, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+2*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+2*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+4*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+4*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, x.+6*pi, y, Z, levels=[0], colorbar=:none, line=(3, :black));
    contour!(p, -reverse(x.+6*pi), y, Z, levels=[0], colorbar=:none, line=(3, :black));
end 
